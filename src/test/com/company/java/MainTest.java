package com.company.java;

import com.company.java.Main;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test // Аннотация отмечает метод как тест
    public void minOfThreeTest() {
        Main math = new Main();
        assertEquals(3, math.minOfThree(3,4,5));
        //Ошибка в первом ветвлении  if (a > b) нужно  if (a < b)
    }

    @Test // Аннотация отмечает метод как тест
    public void factorialTest() {
        Main math = new Main();
        assertEquals(120, math.factorial(5));
        //Ошибка в переменной цикла: должно быть  n--
    }

    @Test // Аннотация отмечает метод как тест
    public void compareStringTest() {

        String Man = "Ivanov";
        String HisBrother = "Ivanov";

        Main math = new Main();

        assertEquals(true, math.compareString(Man, HisBrother));
        assertEquals(true, math.compareString(Man, "Ivanov"));
        //Не понял, где Вы видите подвох (((
    }

    @Test // Аннотация отмечает метод как тест
    public void concatenateStringTest() {
        Main math = new Main();
        assertEquals("Test", math.concatenateString("T","e","s","t"));
        //Следует: result = result + s;
    }

    @Test //*Находит минимум в одномерном массиве*/
    public void findMinInArrayTest() {
        int[] array = {31, 2, 6, 4, 1};
        Main math = new Main();
        assertEquals(1, math.findMinInArray(array));
        //Выход за границы массива, нужно: for (int i = 1; i < array.length; i++)
    }

    @Test //*сортирует массив по возрастанию*/
    public void sortArrayTest() {
        int[] array = {31, 2, 6, 4, 1};
        int[] arraySort = {1, 2, 4, 6, 31};
        Main math = new Main();
        assertArrayEquals(arraySort,math.sortArray(array));
        //Выход за границы массива, нужно: int i = array.length - 1
        //Ошибка в операторе сравнения: if (array[j] > array[j + 1])
       }

    @Test //*Находит сумму элементов, находящихся ниже главной диагонали*/
    public void sumOfElementsUnderMainDiagonalTest() {
        int[][] array = {{1, 2, 6, 4, 1},
                        {1, 2, 6, 4, 1},
                        {1, 2, 6, 4, 1},
                        {1, 2, 6, 4, 1},
                        {1, 2, 6, 4, 1}};

        Main math = new Main();
        assertEquals(40,math.sumOfElementsUnderMainDiagonal(array));
        //шибок не нашёл :(
    }

    @Test//*Находит количество чисел, являющихся степенями двойки в матрице*/
    public void countPowerOfTwoTesr() {
        int[][] array = {{1, 2, 6, 4, 1},
                {3, 2, 6, 4, 7},
                {3, 2, 6, 4, 3},
                {3, 2, 6, 4, 9},
                {3, 2, 6, 4, 11}};

        Main math = new Main();
        assertEquals(8, math.countPowerOfTwo(array));
        //Оператор поиска элементов степени 2 работает неверно if ((matrix[i][j] | (matrix[i][j] - 1)) == 0)
    }

    @Test(timeout = 1000) // Тест должен пройти за 1 секунду
    public void sleepFor1000Test() throws InterruptedException {

        Main math = new Main();
        math.sleepFor1000();
        //Ничего подозрительного не нашёл ((

    }

    @Test(expected = Exception.class) // Тест должен выбросить NullPointerException
    public void throwExceptionTest() throws Exception {

        Main math = new Main();
        math.throwException();
        //Ничего подозрительного не нашёл (((

    }

}
